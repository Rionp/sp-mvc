package com.sp.mvc.service;

import java.util.List;

import com.sp.mvc.vo.CommentBoardVO;

public interface CommentBoardService {

	int insertCommentBoard(CommentBoardVO cm);
	int updateCommentBoard(CommentBoardVO cm);
	int deleteCommentBoard(CommentBoardVO cm);
	List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cm);
	CommentBoardVO selectCommentBoard(CommentBoardVO cm);
}
