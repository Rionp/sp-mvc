package com.sp.mvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.mvc.dao.CommentBoardDAO;
import com.sp.mvc.service.CommentBoardService;
import com.sp.mvc.vo.CommentBoardVO;
@Service
public class CommentBoardServiceImpl implements CommentBoardService {
	@Autowired
	private CommentBoardDAO cmDAO;
	@Override
	public int insertCommentBoard(CommentBoardVO cm) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateCommentBoard(CommentBoardVO cm) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteCommentBoard(CommentBoardVO cm) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cm) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CommentBoardVO selectCommentBoard(CommentBoardVO cm) {
		return cmDAO.selectCommentBoard(cm);
	}

}
