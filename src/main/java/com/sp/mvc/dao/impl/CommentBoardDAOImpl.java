package com.sp.mvc.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sp.mvc.dao.CommentBoardDAO;
import com.sp.mvc.vo.CommentBoardVO;

@Repository
public class CommentBoardDAOImpl implements CommentBoardDAO {

	@Autowired
	private SqlSessionFactory ssf;
	
	@Override
	public int insertCommentBoard(CommentBoardVO cm) {
		try(SqlSession ss = ssf.openSession()){
			return ss.insert("CommentBoard.insertCommentBoard",cm);
		}
	}

	@Override
	public int updateCommentBoard(CommentBoardVO cm) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteCommentBoard(CommentBoardVO cm) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cm) {
		try(SqlSession ss = ssf.openSession()){
			return ss.selectOne("CommentBoard.selectCommentBoardList",cm);
		}
	}

	@Override
	public CommentBoardVO selectCommentBoard(CommentBoardVO cm) {
		try(SqlSession ss = ssf.openSession()){
			return ss.selectOne("CommentBoard.selectCommentBoard",cm);
		}
	}

}
