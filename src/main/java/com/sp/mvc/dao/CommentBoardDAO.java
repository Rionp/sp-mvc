package com.sp.mvc.dao;

import java.util.List;

import com.sp.mvc.vo.CommentBoardVO;

public interface CommentBoardDAO {

	int insertCommentBoard(CommentBoardVO cm);
	int updateCommentBoard(CommentBoardVO cm);
	int deleteCommentBoard(CommentBoardVO cm);
	List<CommentBoardVO> selectCommentBoardList(CommentBoardVO cm);
	CommentBoardVO selectCommentBoard(CommentBoardVO cm);
}
